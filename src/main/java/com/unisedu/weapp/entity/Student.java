package com.unisedu.weapp.entity;

import lombok.Data;

import java.util.List;

/**
 * @author hanbing
 */
@Data
public class Student {
    private Integer id;
    private String name;
    private String gender;
    private String cardNumber;
    private String mobile;
    private String province;
    private String city;
    private String county;
    private String school;
    private String grade;
    private String email;
    private String className;
    private String openId;
    private String status;
    private List<String> checkbox;
}
